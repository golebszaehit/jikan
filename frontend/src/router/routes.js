import Home from "@/views/Home";

function page (path) {
    return () => import(/* webpackChunkName: '' */ `../views/${path}`).then(m => m.default || m)
}

export default [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/about',
        name: 'about',
        component: page('About.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: page('auth/Register.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: page('auth/Login.vue')
    },

    // user
    {
        path: '/table-time',
        name: 'TableTime',
        component: page('user/TableTime.vue')
    },
    {
        path: '/settings',
        name: 'settings',
        component: page('user/Settings.vue')
    },

    //admin
    {
        path: '/admin',
        component: page('admin/index.vue'),
        children: [
            {
                path: 'users',
                name: 'admin.AdminUsers',
                component: page('admin/users/UsersList.vue')
            },
        ]
    },
]