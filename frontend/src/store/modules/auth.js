import Vue from 'vue'
import Vuex from 'vuex'
import Cookie from 'js-cookie'
import axios from 'axios'
import _ from 'lodash'
Vue.use(Vuex)

export const state = {
    user: null,
    token: Cookie.get('token')
}

export const getters = {
    user: state => state.user,
    token: state => state.token,
    check: state => state.user !== null,

    isAdmin: state => {
        if (state.user !== null && state.user.role === 'admin')
            return true
        return false
    },

    isUser: state => {
        if (state.user !== null && state.user.role === 'user')
            return true
        return false
    }
}

export const mutations = {
    saveToken(state, payload){
        state.token = payload.token
        const now = new Date()
        now.setTime(now.getTime() + 1 * 3600 * 1000)
        Cookie.set('token', payload.token, { expires: now })
    },

    fetchUserSuccess(state, { user }){
        state.user = user
    },

    fetchUserFailure(state){
        state.token = null
        Cookies.remove('token')
    },

    logout(state){
        state.user = null
        state.token = null

        Cookie.remove('token')
    }
}

export const actions = {
    saveToken({ commit }, token){
        commit('saveToken', token)
    },

    async fetchUser({ commit }){
        try {
            const { data } = await axios.get('/api/user')
            commit('fetchUserSuccess', { user: data.user })
        } catch (e) {
            console.log(e)
            commit('fetchUserFailure')
        }
    },

    async logout ({ commit }) {
        try {
            await axios.post('/api/logout')
        } catch (e) { }

        commit('logout')
    },
}