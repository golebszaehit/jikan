import axios from 'axios'
import store from '../../store/'
import router from '../../router/'

// Request interceptor
axios.interceptors.request.use(request => {
    const token = store.getters['auth/token']
    if (token) {
        request.headers.common['x-access-token'] = `${token}`
    }

    return request
})

// Response interceptor
axios.interceptors.response.use(response => response, error => {
    const { status } = error.response

    if (status === 401 && store.getters['auth/check']) {
        store.dispatch('auth/logout')

        router.push({ name: 'login' })
    }

    if (status >= 500) {
        serverError(error.response)
    }

    return Promise.reject(error)
})