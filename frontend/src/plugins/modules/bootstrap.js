import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap'

import '../../assets/scss/style.scss'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)