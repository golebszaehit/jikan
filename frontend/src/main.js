import Vue from 'vue'
import App from './views/App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import './plugins'

import AOS from 'aos'
import 'aos/dist/aos.css'

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: function (h) {
        return h(App)
    },
    mounted() {
        AOS.init({
            offset: 120,
            delay: 500,
            duration: 500,
        })
    },
}).$mount('#app')