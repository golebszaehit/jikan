import store from '../store'

export default (to, from, next) => {
    console.log('middleware guest')
    if (store.getters['auth/check']) {
        next({ name: 'home' })
    } else {
        next()
    }
}