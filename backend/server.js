import express from 'express'
import dotenv from 'dotenv'
import cors from 'cors'

const app = express()
dotenv.config()

app.use(cors())
app.use(express.json())

//Import Routes
require('./routes/web')(app)

const port = process.env.APP_PORT || 5000

app.get('/', (req, res) => {
    res.send(`Hello World!`)
})

app.listen(port, () => {
    console.log(`Server run on port: ${port}`)
})