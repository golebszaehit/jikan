import jwt from "jsonwebtoken"
const tokenKey = process.env.TOKEN_KEY

module.exports = {
    verifyToken: (req, res, next) => {
        const token = req.body.token || req.query.token || req.headers["x-access-token"]

        if (!token) {
            return res.status(403).send({ message: "A token is required for authentication" })
        }
        try {
            const decoded = jwt.verify(token, tokenKey);
            req.user = decoded;
        } catch (err) {
            return res.status(401).send("Invalid Token")
        }
        return next()
    },

    authUser: (req, res) => {
        const token = req.body.token || req.query.token || req.headers["x-access-token"]

        if (!token) {
            return res.status(403).send({ message: "A token is required for authentication" })
        }

        const decoded = jwt.verify(token, tokenKey);
        return res.status(200).send({ user: decoded })
    },

    isAdmin: (req, res, next) => {
        if(req.user.role !== 'admin') return res.status(403).send({ message: "You are not authorized" })
        return next()
    },

    isUser: (req, res, next) => {
        if(req.user.role !== 'user') return res.status(403).send({ message: "You are not authorized" })
        return next()
    }
}