import _ from 'lodash'
import moment from 'moment'

module.exports = {
    responseTimetable: (res, timetable) => {
        let arrayTimetable = []
        _.forEach(timetable, (value) => {
            const splitTime = value.time.split(':')

            arrayTimetable.push({
                date: moment(value.date).local('pl').format('YYYY-MM-DD'),
                time: `${splitTime[0]}:${splitTime[1]}`,
                description: value.description
            })
        })
        arrayTimetable = _.sortBy(arrayTimetable, (n => n.date))
        res.status(200).json(arrayTimetable)
    },

     workedTimeInMonth: (timetable) => {
        let arrayTimetable = []
        _.forEach(timetable, (value) => {
            const splitTime = value.time.split(':')

            arrayTimetable.push(`${splitTime[0]}:${splitTime[1]}`)
        })

        return arrayTimetable
    },

    sumMinutes(myArray) {
        let hours = 0
        let minutes = 0
        let sum = ''
        for(let i in myArray){
            hours += parseInt(myArray[i].substring(0, 2))
            minutes += parseInt(myArray[i].substring(3, 5))
        }

        if(minutes > 59){
            hours += parseInt(minutes / 60)
            minutes = parseInt(minutes % 60)
        }

        sum = hours + ':' + ((minutes >= 10) ? minutes : '0' + minutes)
        return sum
    }
}