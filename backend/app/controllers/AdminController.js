import User from '../models/User'

module.exports = {
    getAllUsers: async (req, res) => {
        await User.getAllUsers((err, users) => {
            res.status(200).send(users)
        })
    },
}