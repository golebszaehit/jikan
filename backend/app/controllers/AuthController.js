import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import User from '../models/User'

const tokenKey = process.env.TOKEN_KEY

module.exports = {
    register: async (req,res) => {
        if(!req.body.email.length){
            return res.status(422).send({
                message: 'Email is required'
            })
        }else{
            await User.findOne(req.body.email, async (err, user) => {
                if (user.length) {
                    res.status(422).send({
                        message: 'This email is already in use!'
                    })
                } else {
                    let hashPassword = await bcrypt.hash(req.body.password, 10);

                    if(req.body.name === undefined || !req.body.name.length){
                        return res.status(422).send({
                            message: 'Name is required'
                        })
                    }else if(req.body.surname === undefined || !req.body.surname.length){
                        return res.status(422).send({
                            message: 'Surname is required'
                        })
                    }else if(req.body.password === undefined || !req.body.password.length){
                        return res.status(422).send({
                            message: 'Password is required'
                        })
                    }else{
                        const newUser = {
                            name: req.body.name,
                            surname: req.body.surname,
                            email: req.body.email,
                            password: hashPassword,
                        }

                        await User.create(newUser, () => {
                            res.status(200).json({
                                message: 'User register!'
                            })
                        })
                    }
                }
            })
        }
    },

    login: async (req, res) => {
        await User.findOne(req.body.email, async (err, user) => {
            if (!user.length) {
                res.status(422).send({
                    message: 'This email does not exist!'
                })
            }else{
                await bcrypt.compare(req.body.password, user[0].password, async (err, isEqual) => {
                    if(err) return res.status(422).json({ error: err })

                    if(isEqual){
                        let roleName = ''
                        let authUser = {}
                        let result

                        await User.getUserRole(user[0].id, (error, role) => {
                            if(error) return res.status(422).json({ error: error })

                            if(role.length){
                                const obj = Object.values(JSON.parse(JSON.stringify(role)));
                                roleName = obj[0].name
                            }
                        })

                        setTimeout(() => {
                            const token = jwt.sign(
                                {
                                    userId: user[0].id,
                                    email: user[0].email,
                                    name: user[0].name,
                                    surname: user[0].surname,
                                    role: roleName
                                },
                                tokenKey,
                                {
                                    expiresIn: "1h"
                                }
                            )

                            authUser = {
                                userId: user[0].id,
                                email: user[0].email,
                                name: user[0].name,
                                surname: user[0].surname,
                                role: roleName
                            }

                            result = res.status(200).json({
                                message: 'Auth user',
                                user: authUser,
                                token: token
                            })
                        }, 200)

                        return result
                    }

                    res.status(422).json({ error: 'Password is invalid' })
                })
            }
        })
    },

    logout: async (req, res) => {
        try {
            req.body.token = ''
            req.query.token = ''
            req.headers["x-access-token"] = ''
            req.token = ''

            return res.status(200).json({ message: 'Logout' })
        }catch (err) {
            res.status(500).json({ error: err })
        }
    }
}