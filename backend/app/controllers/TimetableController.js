import Timetable from '../models/Timetable'
import moment from 'moment'
import { responseTimetable, workedTimeInMonth, sumMinutes } from '../middleware/timetable'

module.exports = {
    getTimetable: async (req, res) => {
        await Timetable.getAll(req.user.userId, (err, timetable) => {
            responseTimetable(res, timetable)
        })
    },

    getTimetableByMonth: async (req, res) => {
        await Timetable.getByMonth(req.user.userId, req.params.month, (err, timetable) => {
            responseTimetable(res, timetable)
        })
    },

    getTimetableByMonthAndYear: async (req, res) => {
        await Timetable.getByMonthAndYear(req.user.userId, req.params.month, req.params.year, (err, timetable) => {
            responseTimetable(res, timetable)
        })
    },

    getSumWorkedTimeInMonth: async (req, res) => {
        await Timetable.getByMonthAndYear(req.user.userId, req.params.month, req.params.year, (err, timetable) => {
            const time = workedTimeInMonth(timetable)

            if(time.length) res.status(200).json(sumMinutes(time))
            else res.status(200).json(0)
        })
    },

    sumWorkedTime: (req, res) => {
        res.status(200).json(sumMinutes(req.body.times))
    },

    addNewTimetable: async (req, res) => {
        if(req.body.date === undefined || !req.body.date.length){
            return res.status(422).send({
                message: 'Date is required'
            })
        }else{
            if(moment(req.body.date, 'YYYY-MM-DD', true).isValid() !== true){
                return res.status(422).send({
                    message: 'Date is invalid'
                })
            }
        }

        if(req.body.time === undefined || !req.body.time.length){
            return res.status(422).send({
                message: 'Time is required'
            })
        }else{
            if(moment(req.body.time, 'HH:mm', true).isValid() !== true){
                return res.status(422).send({
                    message: 'Time is invalid'
                })
            }
        }

        const newTimetable = {
            id_user: req.user.userId,
            date: req.body.date,
            time: req.body.time,
            description: req.body.description ? req.body.description : ''
        }
        await Timetable.create(newTimetable, () => {
            res.status(200).json({
                message: 'Add new timetable'
            })
        })
    },

    updateTimetable: async (req, res) => {
        if(req.body.date === undefined || !req.body.date.length){
            return res.status(422).send({
                message: 'Date is required'
            })
        }else{
            if(moment(req.body.date, 'YYYY-MM-DD', true).isValid() !== true){
                return res.status(422).send({
                    message: 'Date is invalid'
                })
            }
        }

        if(req.body.time === undefined || !req.body.time.length){
            return res.status(422).send({
                message: 'Time is required'
            })
        }else{
            if(moment(req.body.time, 'HH:mm', true).isValid() !== true){
                return res.status(422).send({
                    message: 'Time is invalid'
                })
            }
        }

        const description = req.body.description ?? ''

        await Timetable.findOne(req.body.date, req.user.userId, async (error, find) => {
            if(!find.length){
                return res.status(422).send({
                    message: 'This date not exists'
                })
            }else{
                await Timetable.update(req.body.time, description, req.body.date, req.user.userId, () => {
                    res.status(200).json({
                        message: `Update time on ${req.body.date}`
                    })
                })
            }
        })
    }
}