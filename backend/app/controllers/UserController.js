import User from '../models/User'

module.exports = {
    getUser: async (req, res) => {
        await User.getUser(req.user.userId, (err, user) => {
            res.status(200).send(user[0])
        })
    },

    updateUser: async (req, res) => {
        await User.updateUser(req.user.userId, req.body.name, req.body.surname, req.body.province, req.body.city, req.body.street, req.body.house_number, (err, user) => {
            if(req.body.name === '' || req.body.surname === '' || req.body.province === '' || req.body.city === '' || req.body.street === '' || req.body.house_number === ''){
                res.status(422).send({
                    message: 'Please complete all fields'
                })
            }else{
                res.status(200).send({
                    message: 'Updated user'
                })
            }
        })
    },

    getSettings: async (req, res) => {
        await User.getSettings(req.user.userId, (err, user) => {
            if(user.length){
                const splitTime = user[0].hours_daily.split(':')
                res.status(200).send({
                    hours_daily: `${splitTime[0]}:${splitTime[1]}`,
                    working_saturday: user[0].working_saturday,
                    working_sunday: user[0].working_sunday
                })
            }else{
                res.status(200).send({
                    hours_daily: '',
                    working_saturday: 0,
                    working_sunday: 0
                })
            }
        })
    },

    addSettings: async (req, res) => {
        await User.addSettings(req.user.userId, req.body.hours_daily, req.body.working_saturday, req.body.working_sunday, (err, user) => {
            res.status(200).send({
                message: 'Added settings'
            })
        })
    },

    updateSettings: async (req, res) => {
        await User.updateSettings(req.user.userId, req.body.hours_daily, req.body.working_saturday, req.body.working_sunday, (err, user) => {
            res.status(200).send({
                message: 'Update settings'
            })
        })
    },
}