import db from '../database/connection'

module.exports = {
    getAll: (user, callback) => {
        db.query(`SELECT date, time, description FROM timetable WHERE id_user = ${user}`, callback)
    },

    getByMonth: (user, month, callback) => {
        db.query(`SELECT date, time, description FROM timetable WHERE id_user = ${user} AND month(date) = ${month}`, callback)
    },

    getByMonthAndYear: (user, month, year, callback) => {
        db.query(`SELECT date, time, description FROM timetable WHERE id_user = ${user} AND month(date) = ${month} AND year(date) = ${year}`, callback)
    },

    create: (data, callback) => {
        db.query('INSERT INTO timetable SET ?', data, callback)
    },

    update: (time, description, date, id_user, callback) => {
        db.query(`UPDATE timetable SET time = '${time}', description = '${description}' WHERE date = '${date}' AND id_user = ${id_user}`, callback)
    },

    findOne: (date, id_user, callback) => {
        db.query(`SELECT * FROM timetable WHERE date = '${date}' AND id_user = ${id_user}`, callback)
    },
}