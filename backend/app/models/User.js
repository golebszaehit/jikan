import moment from 'moment'
import db from '../database/connection'

let userRoleId, adminRoleId

const getUserRoleId = new Promise((success, failure) => {
    db.query("SELECT id FROM roles WHERE name = 'user'", (err, row) => {
        if(err){
            failure(err)
        }else{
            const obj = Object.values(JSON.parse(JSON.stringify(row)))
            const id = obj[0].id
            success(id)
        }
    })
}).then(result => userRoleId = result)

const getAdminRoleId = new Promise((success, failure) => {
    db.query("SELECT id FROM roles WHERE name = 'admin'", (err, row) => {
        if(err){
            failure(err)
        }else{
            const obj = Object.values(JSON.parse(JSON.stringify(row)))
            const id = obj[0].id
            success(id)
        }
    })
}).then(result => adminRoleId = result)

module.exports = {
    getAllUsers: (callback) => {
        db.query('SELECT id, name, surname, email FROM users', callback)
    },

    create: (data, callback) => {
        db.query('INSERT INTO users SET ?', data, callback)

        setTimeout(() => {
            db.query(`SELECT id FROM users WHERE LOWER(email) = LOWER(${db.escape(data.email)})`, (err, row) => {
                const obj = Object.values(JSON.parse(JSON.stringify(row)));
                const userId = obj[0].id
                setTimeout(() => {
                    const sql = `INSERT INTO user_role (id_user, id_role) VALUES (${userId}, ${userRoleId})`
                    db.query(sql)
                }, 500)
            })
        }, 500)
    },

    getUserRole: (idUser, callback) => {
        const sql = "SELECT roles.name FROM roles INNER JOIN `user_role` ON roles.id = user_role.id_role WHERE user_role.id_user = " + idUser
        db.query(sql, callback)
    },

    findOne: (findEmail, callback) => {
        db.query(
            `SELECT * FROM users WHERE LOWER(email) = LOWER(${db.escape(findEmail)})`,
            callback
        )
    },

    getUser: (id_user, callback) => {
        const sql = `
            SELECT users.name, users.surname, user_address.province, user_address.city, user_address.street, user_address.house_number
            FROM users
            LEFT JOIN user_address ON users.id = user_address.id_user
            WHERE users.id = ${id_user}
        `
        db.query(sql, callback)
    },

    updateUser: (id_user, name, surname, province, city, street, house_number, callback) => {
        const updated = moment().locale('pl').format('YYYY-MM-DD HH:mm:ss')

        db.query(`SELECT id_user FROM user_address WHERE id_user = ${id_user}`, (err, user) => {
            if(user.length){
                db.query(`UPDATE users SET name = '${name}', surname = '${surname}', updated = '${updated}' WHERE id = ${id_user}`)
                db.query(`UPDATE user_address SET province = '${province}', city = '${city}', street = '${street}', house_number = '${house_number}' WHERE id_user = ${id_user}`, callback)
            }else{
                const sql = `INSERT INTO user_address (id_user, province, city, street, house_number) VALUES ('${id_user}', '${province}', '${city}', '${street}', '${house_number}')`
                db.query(`UPDATE users SET name = '${name}', surname = '${surname}', updated = '${updated}' WHERE id = ${id_user}`)
                db.query(sql, callback)
            }
        })
    },

    getSettings: (id_user, callback) => {
        db.query(`SELECT hours_daily, working_saturday, working_sunday FROM user_setting WHERE id_user = ${id_user}`, callback)
    },

    addSettings: (id_user, hours_daily, working_saturday, working_sunday, callback) => {
        db.query(`INSERT INTO user_setting (id_user, hours_daily, working_saturday, working_sunday) VALUES ('${id_user}', '${hours_daily}', ${working_saturday}, ${working_sunday})`, callback)
    },

    updateSettings: (id_user, hours_daily, working_saturday, working_sunday, callback) => {
        db.query(`UPDATE user_setting SET hours_daily = '${hours_daily}', working_saturday = ${working_saturday}, working_sunday = ${working_sunday}  WHERE id_user = ${id_user}`, callback)
    },
}
