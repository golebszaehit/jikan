import express from 'express'
import TimetableController from '../app/controllers/TimetableController'
import UserController from '../app/controllers/UserController'
import authMiddleware from '../app/middleware/auth'

const router = express.Router()

//timetable
router.post('/add-new-timetable', authMiddleware.verifyToken, authMiddleware.isUser, TimetableController.addNewTimetable)
router.get('/timetable', authMiddleware.verifyToken, authMiddleware.isUser, TimetableController.getTimetable)
router.get('/timetable/:month', authMiddleware.verifyToken, authMiddleware.isUser, TimetableController.getTimetableByMonth)
router.get('/timetable/:year/:month', authMiddleware.verifyToken, authMiddleware.isUser, TimetableController.getTimetableByMonthAndYear)
router.get('/timetable-sum-worked-time/:year/:month', authMiddleware.verifyToken, authMiddleware.isUser, TimetableController.getSumWorkedTimeInMonth)
router.patch('/timetable-update', authMiddleware.verifyToken, authMiddleware.isUser, TimetableController.updateTimetable)

router.post('/sum-times', authMiddleware.verifyToken, authMiddleware.isUser, TimetableController.sumWorkedTime)

//settings
router.get('/info', authMiddleware.verifyToken, authMiddleware.isUser, UserController.getUser)
router.post('/update', authMiddleware.verifyToken, authMiddleware.isUser, UserController.updateUser)
router.get('/settings', authMiddleware.verifyToken, authMiddleware.isUser, UserController.getSettings)
router.post('/add-settings', authMiddleware.verifyToken, authMiddleware.isUser, UserController.addSettings)
router.patch('/update-settings', authMiddleware.verifyToken, authMiddleware.isUser, UserController.updateSettings)

module.exports = router