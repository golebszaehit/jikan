import express from 'express'
import AuthController from '../app/controllers/AuthController'
import authMiddleware from '../app/middleware/auth'

const router = express.Router()

router.post('/register', AuthController.register)
router.post('/login', AuthController.login)
router.post('/logout', AuthController.logout)
router.get('/user', authMiddleware.authUser)

module.exports = router