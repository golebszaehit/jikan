import express from 'express'
import AdminController from '../app/controllers/AdminController'
import authMiddleware from '../app/middleware/auth'

const router = express.Router()

router.get('/users', authMiddleware.verifyToken, authMiddleware.isAdmin, AdminController.getAllUsers)

module.exports = router