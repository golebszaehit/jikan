import authRouter from './auth'
import userRouter from './user'
import adminRouter from './admin'

module.exports = (app) => {
    app.use('/api', authRouter)
    app.use('/api/admin', adminRouter)
    app.use('/api/user', userRouter)
}